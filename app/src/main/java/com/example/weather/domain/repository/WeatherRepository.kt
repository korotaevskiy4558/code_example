package com.example.weather.domain.repository

import com.example.weather.domain.models.LatLon
import com.example.weather.domain.models.Weather
import io.reactivex.Single

interface WeatherRepository {
    fun getCurrentWeather(latLon: LatLon):  Single<Weather>
}