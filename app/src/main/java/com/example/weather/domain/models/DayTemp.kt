package com.example.weather.domain.models

data class DayTemp(val temp: Double,
                   val date: String)