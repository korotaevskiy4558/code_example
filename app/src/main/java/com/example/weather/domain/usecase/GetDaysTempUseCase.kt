package com.example.weather.domain.usecase

import com.example.weather.domain.base.SingleUseCase
import com.example.weather.domain.models.DayTemp
import com.example.weather.domain.models.LatLon
import com.example.weather.domain.repository.DaysTempRepository
import io.reactivex.Single

class GetDaysTempUseCase(private val daysTempRepository: DaysTempRepository) : SingleUseCase<List<DayTemp>, LatLon>() {
    override fun useCaseExecution(params: LatLon): Single<List<DayTemp>> = daysTempRepository.getDaysTemp(params)
}