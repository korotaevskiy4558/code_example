package com.example.weather.domain.repository

import com.example.weather.domain.models.DayTemp
import com.example.weather.domain.models.LatLon
import io.reactivex.Single

interface DaysTempRepository {
    fun getDaysTemp(latLon: LatLon): Single<List<DayTemp>>
}