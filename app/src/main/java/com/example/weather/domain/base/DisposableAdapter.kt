package com.example.weather.domain.base

interface DisposableAdapter {
    fun clear()
}