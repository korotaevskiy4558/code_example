package com.example.weather.domain.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import timber.log.Timber

abstract class BaseUseCase<T> : DisposableAdapter  {

    protected var disposables = CompositeDisposable()

    protected fun disposableSingleObserver(
        onNext: (T) -> Unit,
        onError: (Throwable) -> Unit = {}
    ): DisposableSingleObserver<T> {

        return object : DisposableSingleObserver<T>() {
            override fun onSuccess(value: T) {
                Timber.i("onNext...")
                onNext.invoke(value)
            }

            override fun onError(e: Throwable) {
                Timber.e( "onError... ${e.localizedMessage}")
                onError.invoke(e)
            }
        }
    }

    protected fun disposableCompletableObserver(
        onComplete: () -> Unit,
        onError: (Throwable) -> Unit = {}
    ): DisposableCompletableObserver {

        return object : DisposableCompletableObserver() {

            override fun onComplete() {
                Timber.i("onComplete...")
                onComplete.invoke()
            }

            override fun onError(e: Throwable) {
                Timber.e( "onError... ${e.localizedMessage}")
                onError.invoke(e)
            }
        }
    }

    override fun clear() {
        if(!disposables.isDisposed){
            disposables.clear()
        }

    }


}