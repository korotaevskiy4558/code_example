package com.example.weather.domain.models

import java.io.Serializable

data class Weather(
    var id: Int,
    val lon: Double,
    val lat: Double,
    val temp: Double,
    val tempMin: Double,
    val tempMax: Double,
    val name: String,
    val humidity : Int,
    val speed : Double
): Serializable