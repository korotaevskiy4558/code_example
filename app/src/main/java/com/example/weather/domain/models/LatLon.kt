package com.example.weather.domain.models

data class LatLon(
    val lat: Double,
    val lon: Double
)