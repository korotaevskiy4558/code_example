package com.example.weather.domain.usecase

import com.example.weather.domain.base.SingleUseCase
import com.example.weather.domain.models.LatLon
import com.example.weather.domain.models.Weather
import com.example.weather.domain.repository.WeatherRepository
import io.reactivex.Single

class GetCurrentWeatherUseCase(private val weatherRepository: WeatherRepository) : SingleUseCase< Weather, LatLon>() {
    override fun useCaseExecution(params: LatLon):  Single<Weather> = weatherRepository.getCurrentWeather(params)
}