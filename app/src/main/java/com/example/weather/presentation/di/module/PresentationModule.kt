package com.example.weather.presentation.di.module

import com.example.weather.presentation.ui.main.WeatherViewModel
import com.example.weather.presentation.ui.util.NetworkCheck
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    single { NetworkCheck(androidContext()) }
    viewModel { WeatherViewModel(get(),get(), get())}
}