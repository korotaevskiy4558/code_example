package com.example.weather.presentation.ui.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.IntentSender
import android.location.LocationManager
import android.os.Looper
import android.widget.Toast
import com.example.weather.domain.models.LatLon
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import timber.log.Timber


class GpsUtils(private val context: Context) {
    private val settingsClient: SettingsClient = LocationServices.getSettingsClient(context)
    private val locationSettingsRequest: LocationSettingsRequest
    private val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private val locationRequest: LocationRequest
    private val fusedLocationProviderClient: FusedLocationProviderClient =  LocationServices.getFusedLocationProviderClient(context)
    lateinit var locationCallback: LocationCallback

    init {
        locationRequest = LocationRequest()
            .apply {
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                interval = 5000
                fastestInterval = 3000
                smallestDisplacement = 10f
            }
        locationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).setAlwaysShow(true).build()
    }

    fun turnGPSOn() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            settingsClient
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(context as Activity) {}
                .addOnFailureListener(context) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            val rae = e as ResolvableApiException
                            rae.startResolutionForResult(context,
                                GPS_REQUEST
                            )
                        } catch (sie: IntentSender.SendIntentException) {
                            Timber.i("PendingIntent unable to execute request.")
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage =
                                "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
                            Timber.e(errorMessage)
                            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }
                }
        }
    }

    @SuppressLint("MissingPermission")
    fun updateLocation(){
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }

    fun buildLocationCallBack(onGpsListener: OnGpsListener?) {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult?.locations?.get(locationResult.locations.size - 1)?.let{
                    onGpsListener?.currentLocation(LatLon(it.latitude, it.longitude))
                }
                removeLocationProviderClient()
            }
        }
    }

    fun removeLocationProviderClient(){
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    interface OnGpsListener {
        fun currentLocation(latLon: LatLon?)
    }
    companion object{
        const val GPS_REQUEST = 333
    }
}