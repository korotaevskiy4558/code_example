package com.example.weather.presentation.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weather.R


import com.example.weather.domain.models.LatLon
import com.example.weather.presentation.ui.extensions.toast
import com.example.weather.presentation.ui.main.adapter.WeatherAdapter
import com.example.weather.presentation.ui.util.GpsUtils
import kotlinx.android.synthetic.main.activity_weather.*

import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class WeatherActivity : AppCompatActivity(), GpsUtils.OnGpsListener {
    private val mainViewModel by viewModel<WeatherViewModel>()
    private val gpsUtils: GpsUtils by lazy { GpsUtils(this) }
    private val weatherAdapter: WeatherAdapter by lazy { setRecyclerView() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)
        observe()
        checkPermission()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        toast("Permission granted")
                        getCurrentLocation()
                    } else
                        toast("Permission denied")
                    setDefaultLocation()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        gpsUtils.removeLocationProviderClient()
    }

    private fun observe() {
        observeCurrentWeather()
        observeDaysTemp()
        observeNetworkState()
    }

    private fun observeDaysTemp() {
        mainViewModel.listDaysTemp.observe(this, Observer { list ->
            weatherAdapter.update(list)
        })
    }

    private fun setDefaultLocation() {
        val location = LatLon(50.431782, 30.516382)
        mainViewModel.getCurrentWeather(location)
        mainViewModel.getDaysTemp(location)
    }

    private fun setRecyclerView(): WeatherAdapter {
        val adpter = WeatherAdapter()
        recyclerViewWeather.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(this@WeatherActivity)
            adapter = adpter
        }
        return adpter
    }

    private fun observeNetworkState() {
        mainViewModel.networkState.observe(this, Observer { isConect ->
            if (!isConect) {
                toast("no internet connection")
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun observeCurrentWeather() {
        mainViewModel.currentWeather.observe(this, Observer {
            Timber.i(it.toString())
            textViewCurrentTempValue.text = "${it.temp}C"
            textViewMinCurrentTempValue.text = "${it.tempMin}C"
            textViewMaxCurrentTempValue.text = "${it.tempMax}C"
            textViewLocationValue.text = it.name
            textViewHumidityValue.text = "${it.humidity}%"
            textViewWindValue.text = "${it.speed}km/h"
        })
    }

    private fun checkPermission() {
        if (ActivityCompat.checkSelfPermission(
                this@WeatherActivity, Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this@WeatherActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@WeatherActivity,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                REQUEST_CODE
            )
        } else {
            getCurrentLocation()
        }

    }

    override fun currentLocation(latLon: LatLon?) {
        latLon?.let { mainViewModel.getCurrentWeather(it) }
        latLon?.let { mainViewModel.getDaysTemp(it) }
    }

    private fun getCurrentLocation() {
        gpsUtils.turnGPSOn()
        gpsUtils.buildLocationCallBack(this)
        gpsUtils.updateLocation()
    }

    companion object {
        const val REQUEST_CODE = 1000
    }


}
