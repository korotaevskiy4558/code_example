package com.example.weather.presentation.di.module

import android.content.Context
import androidx.room.Room
import com.example.weather.BuildConfig
import com.example.weather.data.repository.DaysTempRepositoryImpl
import com.example.weather.data.repository.WeatherRepositoryImpl
import com.example.weather.data.source.local.room.WeatherDatabase
import com.example.weather.data.source.local.source.day_temp.DayTempLocalSource
import com.example.weather.data.source.local.source.day_temp.DayTempLocalSourceImpl
import com.example.weather.data.source.local.source.weather.WeatherLocalSource
import com.example.weather.data.source.local.source.weather.WeatherLocalSourceImpl
import com.example.weather.data.source.remote.retrofit.WeatherService
import com.example.weather.data.source.remote.source.days_temp.DaysTempRemoteSource
import com.example.weather.data.source.remote.source.days_temp.DaysTempRemoteSourceImpl
import com.example.weather.data.source.remote.source.weather.WeatherRemoteSource
import com.example.weather.data.source.remote.source.weather.WeatherRemoteSourceImpl
import com.example.weather.domain.repository.DaysTempRepository
import com.example.weather.domain.repository.WeatherRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

const val BASE_URL = "https://api.openweathermap.org/"
const val DATABASE_NAME = "weather_database"

val dataModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { provideWeatherService(get()) }
    single { provideWeatherDatabase(androidContext()) }
    single { WeatherLocalSourceImpl(get()) as WeatherLocalSource }
    single { WeatherRemoteSourceImpl(get()) as WeatherRemoteSource }
    single { WeatherRepositoryImpl(get(), get()) as WeatherRepository }
    single { DaysTempRemoteSourceImpl(get()) as DaysTempRemoteSource }
    single { DayTempLocalSourceImpl(get()) as DayTempLocalSource }
    single { DaysTempRepositoryImpl(get(), get()) as DaysTempRepository }
}

fun provideOkHttpClient(): OkHttpClient = OkHttpClient().newBuilder()
    .addInterceptor(HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger{
        override fun log(message: String) {
            Timber.tag("OkHttp").i(message)
        }

    }).apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
        else HttpLoggingInterceptor.Level.NONE
        level = HttpLoggingInterceptor.Level.BODY
    })
    .build()

fun provideRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .client(client)
    .build()

fun provideWeatherService(retrofit: Retrofit): WeatherService = retrofit.create(WeatherService::class.java)

fun provideWeatherDatabase(context: Context) = Room.databaseBuilder(
    context,
    WeatherDatabase::class.java,
    DATABASE_NAME)
    . fallbackToDestructiveMigration ()
    .build()
