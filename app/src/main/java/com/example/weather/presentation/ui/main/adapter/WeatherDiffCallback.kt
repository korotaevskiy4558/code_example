package com.example.weather.presentation.ui.main.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.weather.domain.models.DayTemp

class WeatherDiffCallback (private val oldList: List<DayTemp>, private val newList: List<DayTemp>): DiffUtil.Callback() {


    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int= newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldDayTemp = oldList.get(oldItemPosition)
        val newDayTemp = newList.get(newItemPosition)
        return oldDayTemp.date.equals(newDayTemp.date) && oldDayTemp.temp == newDayTemp.temp
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean  = false
}