package com.example.weather.presentation.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.weather.domain.models.DayTemp
import com.example.weather.domain.models.LatLon
import com.example.weather.domain.models.Weather
import com.example.weather.domain.usecase.GetCurrentWeatherUseCase
import com.example.weather.domain.usecase.GetDaysTempUseCase
import com.example.weather.presentation.ui.base.BaseViewModel
import com.example.weather.presentation.ui.util.NetworkCheck
import timber.log.Timber

class WeatherViewModel(
    protected val getCurrentWeatherUseCase: GetCurrentWeatherUseCase,
    protected val getDaysTempUseCase: GetDaysTempUseCase,
    private val networkCheck: NetworkCheck
) : BaseViewModel(getCurrentWeatherUseCase, getDaysTempUseCase) {


    private val _networkState: MutableLiveData<Boolean> = MutableLiveData()
    private val _currentWeather: MutableLiveData<Weather> = MutableLiveData()
    private val _listDaysTemp: MutableLiveData<List<DayTemp>> = MutableLiveData()

    val currentWeather: LiveData<Weather>
        get() = _currentWeather
    val networkState: LiveData<Boolean>
        get() = _networkState
    val listDaysTemp: LiveData<List<DayTemp>>
        get() = _listDaysTemp

    fun getCurrentWeather(latLon: LatLon){
        Timber.i("Executing GetCurrentWeatherUseCase...")
        _networkState.value = networkCheck.isOnline
        getCurrentWeatherUseCase.execute(
            {locationWeather ->  _currentWeather.value = locationWeather},
            {throwable ->  Timber.e(throwable)},
            latLon)
    }

    fun getDaysTemp(latLon: LatLon){
        Timber.i("Executing GetDaysTempUseCase...")
        _networkState.value = networkCheck.isOnline
        getDaysTempUseCase.execute(
            {list ->  _listDaysTemp.value = list},
            {throwable ->  Timber.e(throwable)},
            latLon)
    }
}