package com.example.weather.presentation.ui.base

import androidx.lifecycle.ViewModel
import com.example.weather.domain.base.DisposableAdapter
import timber.log.Timber

abstract class BaseViewModel constructor(vararg val disposables: DisposableAdapter) : ViewModel() {

    override fun onCleared() {
        super.onCleared()
        for(disposable in disposables){
            disposable.clear()
        }
        Timber.i("Use cases disposed")
    }

}