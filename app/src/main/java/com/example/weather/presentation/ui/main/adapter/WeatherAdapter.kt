package com.example.weather.presentation.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.R
import com.example.weather.domain.models.DayTemp

class WeatherAdapter : RecyclerView.Adapter<WeatherHolder>() {
    private var items: MutableList<DayTemp> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_weather, parent, false)
        return WeatherHolder(view)
    }

    override fun onBindViewHolder(stockHolder: WeatherHolder, i: Int) {
        stockHolder.bindData(items[i])
    }

    override fun getItemCount(): Int = items.size

    fun update(listWeather: List<DayTemp>){
        val diffResult = DiffUtil.calculateDiff(WeatherDiffCallback(items, listWeather), false)
        items.clear()
        items.addAll(listWeather)
        diffResult.dispatchUpdatesTo(this)
    }

}