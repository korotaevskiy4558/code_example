package com.example.weather.presentation.ui.main.adapter

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.weather.domain.models.DayTemp
import kotlinx.android.synthetic.main.item_weather.view.*

class WeatherHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @SuppressLint("SetTextI18n")
    fun bindData(item: DayTemp) {
        itemView.textViewTempValue.text = "${item.temp}C"
        itemView.textViewDate.text = item.date
    }


}