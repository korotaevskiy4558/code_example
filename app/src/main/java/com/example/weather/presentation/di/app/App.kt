package com.example.weather.presentation.di.app

import android.app.Application
import com.example.weather.presentation.di.module.dataModule
import com.example.weather.presentation.di.module.domainModule
import com.example.weather.presentation.di.module.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        startKoin {
            androidContext(this@App)
            modules(listOf(presentationModule, domainModule, dataModule))
        }
    }
}