package com.example.weather.presentation.di.module

import com.example.weather.domain.usecase.GetCurrentWeatherUseCase
import com.example.weather.domain.usecase.GetDaysTempUseCase
import org.koin.dsl.module

val domainModule = module {
    single { GetCurrentWeatherUseCase(get()) }
    single { GetDaysTempUseCase(get()) }
}