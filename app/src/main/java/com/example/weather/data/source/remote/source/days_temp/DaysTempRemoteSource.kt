package com.example.weather.data.source.remote.source.days_temp

import com.example.weather.domain.models.DayTemp
import com.example.weather.domain.models.LatLon
import io.reactivex.Single

interface DaysTempRemoteSource {
    fun getDaysTemp(latLon: LatLon): Single<List<DayTemp>>
}