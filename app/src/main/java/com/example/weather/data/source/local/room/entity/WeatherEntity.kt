package com.example.weather.data.source.local.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "location_weather")
data class WeatherEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    val lon: Double,
    val lat: Double,
    val temp: Double,
    val tempMin: Double,
    val tempMax: Double,
    val name: String,
    val humidity : Int,
    val speed : Double)