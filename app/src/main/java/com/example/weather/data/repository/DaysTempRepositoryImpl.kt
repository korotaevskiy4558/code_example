package com.example.weather.data.repository

import com.example.weather.data.source.local.source.day_temp.DayTempLocalSource
import com.example.weather.data.source.remote.source.days_temp.DaysTempRemoteSource
import com.example.weather.domain.models.DayTemp
import com.example.weather.domain.models.LatLon
import com.example.weather.domain.repository.DaysTempRepository
import io.reactivex.Single

class DaysTempRepositoryImpl (private val daysTempRemoteSource: DaysTempRemoteSource,private val dayTempLocalSource: DayTempLocalSource) : DaysTempRepository{

    override fun getDaysTemp(latLon: LatLon): Single<List<DayTemp>> =
        daysTempRemoteSource.getDaysTemp(latLon)
            .flatMap{ list -> dayTempLocalSource.updateData(list)}
            .onErrorResumeNext { dayTempLocalSource.getDaysTemp() }
}
