package com.example.weather.data.source.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.weather.data.source.local.room.entity.WeatherEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface WeatherDao {

    @Query("SELECT * FROM location_weather WHERE id <>  0 ORDER BY name ASC")
    fun getListWeather(): Single<List<WeatherEntity>>

    @Query("SELECT * FROM location_weather WHERE id = 0")
    fun getCurrentWeather(): Single<WeatherEntity>

    @Query("SELECT * FROM location_weather WHERE id = :id")
    fun getWeatherBy(id: Int): Single<List<WeatherEntity>>

    @Query("DELETE FROM location_weather WHERE id = :id")
    fun deleteByUserId(id: Int): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertListWeather(listWeather: List<WeatherEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeather(weather: WeatherEntity)
}