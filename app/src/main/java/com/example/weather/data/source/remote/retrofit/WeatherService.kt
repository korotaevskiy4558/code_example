package com.example.weather.data.source.remote.retrofit

import com.example.weather.data.source.remote.dto.DayTempDto
import com.example.weather.data.source.remote.dto.WeatherDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("data/2.5/weather")
    fun getWeatherByLatLon(
        @Query("lat") lat: Double = 37.62,
        @Query("lon") lon: Double = 55.75,
        @Query("units") units: String = "metric",
        @Query("appid") appId: String = "2c8098af2935af9c9dab0824e882302e"
    ): Single<WeatherDto>

    @GET("data/2.5/forecast")
    fun getWeatherHourlyTemp(
        @Query("lat") lat: Double = 37.62,
        @Query("lon") lon: Double = 55.75,
        @Query("units") units: String = "metric",
        @Query("cnt") count: Int = 40,
        @Query("appid") appId: String = "2c8098af2935af9c9dab0824e882302e"
    ): Single<DayTempDto>
}