package com.example.weather.data.source.remote.source.weather

import com.example.weather.data.mapper.mapRemoteWeatherToDomain
import com.example.weather.data.source.remote.retrofit.WeatherService
import com.example.weather.domain.models.LatLon
import com.example.weather.domain.models.Weather
import io.reactivex.Single
import timber.log.Timber

class WeatherRemoteSourceImpl(private val weatherService: WeatherService):
    WeatherRemoteSource {
    override fun getWeatherByLatLon(latLon: LatLon): Single<Weather> = weatherService.getWeatherByLatLon(latLon.lat, latLon.lon)
        .doOnSuccess{ Timber.i(it.toString())}
        .map { locationWeatherDto ->
            mapRemoteWeatherToDomain(locationWeatherDto)
        }

}