package com.example.weather.data.source.remote.source.weather

import com.example.weather.domain.models.LatLon
import com.example.weather.domain.models.Weather
import io.reactivex.Single

interface WeatherRemoteSource {
    fun getWeatherByLatLon(latLon: LatLon): Single<Weather>
}