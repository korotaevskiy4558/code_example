package com.example.weather.data.source.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.weather.data.source.local.room.entity.DayTempEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface DayTempDao {

    @Query("SELECT * FROM day_temp")
    fun getAll(): Single<List<DayTempEntity>>

    @Query("DELETE FROM day_temp")
    fun delete(): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(listWeather: List<DayTempEntity>)

}