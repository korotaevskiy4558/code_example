package com.example.weather.data.source.remote.dto

import com.google.gson.annotations.SerializedName


data class DayTempDto(@SerializedName("list") val list: List<ListDayTempDto>)

