package com.example.weather.data.source.local.source.day_temp

import com.example.weather.data.mapper.mapDomainDayTempToLocal
import com.example.weather.data.mapper.mapLocalDayTempToDomain
import com.example.weather.data.source.local.room.WeatherDatabase
import com.example.weather.domain.models.DayTemp
import io.reactivex.Completable
import io.reactivex.Single

class DayTempLocalSourceImpl(private val weatherDatabase: WeatherDatabase)
    : DayTempLocalSource {

    override fun getDaysTemp(): Single<List<DayTemp>> =
        weatherDatabase
            .dayTempDao()
            .getAll()
            .toObservable()
            .flatMapIterable { list ->  list}
            .map{ mapLocalDayTempToDomain(it)}
            .toList()


    override fun updateData(list: List<DayTemp>): Single<List<DayTemp>>  =
         Single.just(list)
            .toObservable()
            .flatMapIterable { listDayTemp -> listDayTemp }
            .map { weatherEntity -> mapDomainDayTempToLocal(weatherEntity) }
            .toList()
            .doOnSuccess { weatherDatabase.dayTempDao().delete() }
            .flatMapCompletable { listDayTempEntity ->
                Completable.fromAction {
                    weatherDatabase.dayTempDao().insert(listDayTempEntity)
                }
            }.andThen(Single.just(list))

}