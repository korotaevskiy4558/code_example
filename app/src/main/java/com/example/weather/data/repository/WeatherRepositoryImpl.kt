package com.example.weather.data.repository


import com.example.weather.data.source.local.source.weather.WeatherLocalSource
import com.example.weather.data.source.remote.source.weather.WeatherRemoteSource
import com.example.weather.domain.models.LatLon
import com.example.weather.domain.models.Weather
import com.example.weather.domain.repository.WeatherRepository

import io.reactivex.Single


class WeatherRepositoryImpl(
    private val weatherRemoteSource: WeatherRemoteSource,
    private val weatherLocalSource: WeatherLocalSource
) : WeatherRepository {

    override fun getCurrentWeather(latLon: LatLon): Single<Weather> =
        weatherRemoteSource
            .getWeatherByLatLon(latLon)
            .doOnSuccess { weatherLocalSource.insertWeather(it) }
            .onErrorResumeNext { weatherLocalSource.getCurrentWeather() }

}