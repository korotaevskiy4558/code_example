package com.example.weather.data.source.remote.source.days_temp

import com.example.weather.data.mapper.mapRemoteDayTempToDomain
import com.example.weather.data.source.remote.dto.DayTempDto
import com.example.weather.data.source.remote.retrofit.WeatherService
import com.example.weather.domain.models.DayTemp
import com.example.weather.domain.models.LatLon
import io.reactivex.Single
import timber.log.Timber

class DaysTempRemoteSourceImpl(private val weatherService: WeatherService) : DaysTempRemoteSource {
    override fun getDaysTemp(latLon: LatLon): Single<List<DayTemp>> =
        weatherService.getWeatherHourlyTemp(latLon.lat, latLon.lon)
            .doOnSuccess{Timber.i(it.toString())}
            .toObservable()
            .map { dayTempDto: DayTempDto ->  dayTempDto.list}
            .flatMapIterable { list ->  list}
            .map{ mapRemoteDayTempToDomain(it)}
            .toList()
}