package com.example.weather.data.source.remote.dto

import com.google.gson.annotations.SerializedName

data class ListWeatherDto(
    @SerializedName("cnt") val cnt: Int,
    @SerializedName("list") val list: List<WeatherDto>
)


