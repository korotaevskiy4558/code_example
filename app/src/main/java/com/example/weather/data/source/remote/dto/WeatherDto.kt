package com.example.weather.data.source.remote.dto

import com.google.gson.annotations.SerializedName

data class WeatherDto (
    @SerializedName("coord") val coord : CoordDto,
    @SerializedName("base") val base : String,
    @SerializedName("main") val main : MainDto,
    @SerializedName("visibility") val visibility : Int,
    @SerializedName("wind") val wind : WindDto,
    @SerializedName("clouds") val clouds : CloudsDto,
    @SerializedName("dt") val dt : Int,
    @SerializedName("sys") val sys : SysDto,
    @SerializedName("timezone") val timezone : Int,
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("cod") val cod : Int
)