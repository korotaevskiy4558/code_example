package com.example.weather.data.source.local.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "day_temp")
data class DayTempEntity(
    val temp: Double,
    val date: String
) {
    @PrimaryKey(autoGenerate = true)
     var id: Int = 0
}