package com.example.weather.data.mapper

import com.example.weather.data.source.local.room.entity.WeatherEntity
import com.example.weather.data.source.remote.dto.WeatherDto
import com.example.weather.domain.models.Weather

val mapRemoteWeatherToDomain:(WeatherDto) -> Weather = { weatherDto ->
    Weather(
        0,
        weatherDto.coord.lon,
        weatherDto.coord.lat,
        weatherDto.main.temp,
        weatherDto.main.temp_min,
        weatherDto.main.temp_max,
        weatherDto.name,
        weatherDto.main.humidity,
        weatherDto.wind.speed)
}

val mapLocalWeatherToDomain: (WeatherEntity) -> Weather = {weatherEntity ->
    Weather(
        weatherEntity.id,
        weatherEntity.lon,
        weatherEntity.lat,
        weatherEntity.temp,
        weatherEntity.tempMin,
        weatherEntity.tempMax,
        weatherEntity.name,
        weatherEntity.humidity,
        weatherEntity.speed)
}

val mapDomainWeatherToLocal: (Weather) -> WeatherEntity = {weather ->
    WeatherEntity(
        weather.id,
        weather.lon,
        weather.lat,
        weather.temp,
        weather.tempMin,
        weather.tempMax,
        weather.name,
        weather.humidity,
        weather.speed)
}