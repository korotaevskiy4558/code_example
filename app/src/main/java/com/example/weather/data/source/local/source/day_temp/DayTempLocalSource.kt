package com.example.weather.data.source.local.source.day_temp

import com.example.weather.domain.models.DayTemp
import io.reactivex.Single

interface DayTempLocalSource {
    fun getDaysTemp(): Single<List<DayTemp>>
    fun updateData(list: List<DayTemp>): Single<List<DayTemp>>
}