package com.example.weather.data.source.remote.dto

import com.google.gson.annotations.SerializedName

data class WindDto (

	@SerializedName("speed") val speed : Double,
	@SerializedName("deg") val deg : Double
)