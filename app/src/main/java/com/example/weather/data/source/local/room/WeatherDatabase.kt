package com.example.weather.data.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.weather.data.source.local.room.dao.DayTempDao
import com.example.weather.data.source.local.room.dao.WeatherDao
import com.example.weather.data.source.local.room.entity.DayTempEntity
import com.example.weather.data.source.local.room.entity.WeatherEntity

@Database(entities = [WeatherEntity::class, DayTempEntity::class], version = 2,  exportSchema = false)
abstract class WeatherDatabase: RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
    abstract fun dayTempDao(): DayTempDao
}