package com.example.weather.data.source.local.source.weather

import com.example.weather.domain.models.Weather
import io.reactivex.Single

interface WeatherLocalSource {
    fun getCurrentWeather(): Single<Weather>
    fun insertWeather(weather: Weather)
}