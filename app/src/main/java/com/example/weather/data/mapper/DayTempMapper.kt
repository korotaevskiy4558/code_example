package com.example.weather.data.mapper

import com.example.weather.data.source.local.room.entity.DayTempEntity
import com.example.weather.data.source.remote.dto.ListDayTempDto
import com.example.weather.domain.models.DayTemp
import java.text.SimpleDateFormat
import java.util.*

val mapRemoteDayTempToDomain: (ListDayTempDto) -> DayTemp = { listDayTempDto ->
    DayTemp(
        listDayTempDto.temp.temp,
        convert(listDayTempDto.dt)
    )
}

val mapLocalDayTempToDomain: (DayTempEntity) -> DayTemp = { listDayTempEntity ->
    DayTemp(
        listDayTempEntity.temp,
        listDayTempEntity.date)
}

val mapDomainDayTempToLocal: (DayTemp) -> DayTempEntity = { listDayTempEntity ->
    DayTempEntity(
        listDayTempEntity.temp,
        listDayTempEntity.date)
}

val convert: (date: Long) -> String = { date ->
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = date*1000
    val format = SimpleDateFormat("E MMM dd HH:mm",Locale.ENGLISH)
    format.format(calendar.time)
}

