package com.example.weather.data.source.remote.dto

import com.google.gson.annotations.SerializedName


data class TempDto(
    @SerializedName("temp") val temp: Double)
