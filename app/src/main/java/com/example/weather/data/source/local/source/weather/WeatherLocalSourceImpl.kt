package com.example.weather.data.source.local.source.weather

import com.example.weather.data.mapper.mapDomainWeatherToLocal
import com.example.weather.data.mapper.mapLocalWeatherToDomain
import com.example.weather.data.source.local.room.WeatherDatabase
import com.example.weather.domain.models.Weather
import io.reactivex.Single

class WeatherLocalSourceImpl(private val weatherDatabase: WeatherDatabase) :
    WeatherLocalSource {


    override fun getCurrentWeather(): Single<Weather> = weatherDatabase
        .weatherDao()
        .getCurrentWeather()
        .map { weatherEntity -> mapLocalWeatherToDomain(weatherEntity) }

    override fun insertWeather(weather: Weather) {
        weatherDatabase
            .weatherDao()
            .insertWeather(mapDomainWeatherToLocal.invoke(weather))
    }

}