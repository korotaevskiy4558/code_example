package com.example.weather.data.source.remote.dto

import com.google.gson.annotations.SerializedName


data class ListDayTempDto(
    @SerializedName("main") val temp: TempDto,
    @SerializedName("dt") val dt: Long
)